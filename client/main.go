package main

import (
	"github.com/astaxie/beego"
	_ "myapp/web/beegoapi/client/routers"
)

func main() {
	//static
	beego.SetStaticPath("/html", "html")
	beego.SetStaticPath("/fonts", "fonts")

	beego.Run()
}
