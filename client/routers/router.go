package routers

import (
	"github.com/astaxie/beego"
	"myapp/web/beegoapi/client/controllers"
)

func init() {
	beego.Router("/", &controllers.MainController{})
	beego.Router("/category", &controllers.CategoryController{})
}
