package main

import (
	"github.com/astaxie/beego"
	"github.com/astaxie/beego/logs"
	"github.com/astaxie/beego/orm"
	_ "github.com/lib/pq"
	_ "myapp/web/beegoapi/server/docs"
	_ "myapp/web/beegoapi/server/routers"
)

var log = logs.NewLogger(10000)

func init() {
	log.SetLogger("console", "")
	//log.EnableFuncCallDepth(true)
	err := orm.RegisterDataBase("default", "postgres", "postgres://greenerp:guotinghuayuan30301@localhost:5432/beegoapi?sslmode=disable")
	if err != nil {
		log.Error("orm.RegisterDataBase error:{%v}", err)
	}
}

func main() {

	// name := "default"
	// force := true
	// verbose := true
	// err := orm.RunSyncdb(name, force, verbose)
	// if err != nil {
	// 	log.Error("orm.RunSyncdb error:{%v}", err)
	// }

	if beego.RunMode == "dev" {
		beego.DirectoryIndex = true
		beego.StaticDir["/swagger"] = "swagger"
	}
	beego.Run()
}
