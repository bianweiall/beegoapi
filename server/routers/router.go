// @APIVersion 1.0.0
// @Title Demo API
// @Description Demo API
// @Contact xxx@xxx.xxx
// @TermsOfServiceUrl http://beego.me/
// @License Apache 2.0
// @LicenseUrl http://www.apache.org/licenses/LICENSE-2.0.html
package routers

import (
	"myapp/web/beegoapi/server/controllers"

	"github.com/astaxie/beego"
)

func init() {
	ns := beego.NewNamespace("/v1",
		beego.NSNamespace("/book",
			beego.NSInclude(&controllers.BookController{}),
		),
		beego.NSNamespace("/author",
			beego.NSInclude(&controllers.AuthorController{}),
		),
	)
	beego.AddNamespace(ns)
}
