package routers

import (
	"github.com/astaxie/beego"
)

func init() {
	
	beego.GlobalControllerRouter["myapp/web/beegoapi/server/controllers:BookController"] = append(beego.GlobalControllerRouter["myapp/web/beegoapi/server/controllers:BookController"],
		beego.ControllerComments{
			"Post",
			`/`,
			[]string{"post"},
			nil})

	beego.GlobalControllerRouter["myapp/web/beegoapi/server/controllers:BookController"] = append(beego.GlobalControllerRouter["myapp/web/beegoapi/server/controllers:BookController"],
		beego.ControllerComments{
			"GetAll",
			`/`,
			[]string{"get"},
			nil})

	beego.GlobalControllerRouter["myapp/web/beegoapi/server/controllers:BookController"] = append(beego.GlobalControllerRouter["myapp/web/beegoapi/server/controllers:BookController"],
		beego.ControllerComments{
			"Get",
			`/:id`,
			[]string{"get"},
			nil})

	beego.GlobalControllerRouter["myapp/web/beegoapi/server/controllers:BookController"] = append(beego.GlobalControllerRouter["myapp/web/beegoapi/server/controllers:BookController"],
		beego.ControllerComments{
			"Put",
			`/:id`,
			[]string{"put"},
			nil})

	beego.GlobalControllerRouter["myapp/web/beegoapi/server/controllers:BookController"] = append(beego.GlobalControllerRouter["myapp/web/beegoapi/server/controllers:BookController"],
		beego.ControllerComments{
			"Delete",
			`/:id`,
			[]string{"delete"},
			nil})

	beego.GlobalControllerRouter["myapp/web/beegoapi/server/controllers:AuthorController"] = append(beego.GlobalControllerRouter["myapp/web/beegoapi/server/controllers:AuthorController"],
		beego.ControllerComments{
			"Post",
			`/`,
			[]string{"post"},
			nil})

	beego.GlobalControllerRouter["myapp/web/beegoapi/server/controllers:AuthorController"] = append(beego.GlobalControllerRouter["myapp/web/beegoapi/server/controllers:AuthorController"],
		beego.ControllerComments{
			"GetAll",
			`/`,
			[]string{"get"},
			nil})

	beego.GlobalControllerRouter["myapp/web/beegoapi/server/controllers:AuthorController"] = append(beego.GlobalControllerRouter["myapp/web/beegoapi/server/controllers:AuthorController"],
		beego.ControllerComments{
			"Get",
			`/:id`,
			[]string{"get"},
			nil})

	beego.GlobalControllerRouter["myapp/web/beegoapi/server/controllers:AuthorController"] = append(beego.GlobalControllerRouter["myapp/web/beegoapi/server/controllers:AuthorController"],
		beego.ControllerComments{
			"Put",
			`/:id`,
			[]string{"put"},
			nil})

	beego.GlobalControllerRouter["myapp/web/beegoapi/server/controllers:AuthorController"] = append(beego.GlobalControllerRouter["myapp/web/beegoapi/server/controllers:AuthorController"],
		beego.ControllerComments{
			"Delete",
			`/:id`,
			[]string{"delete"},
			nil})

}
