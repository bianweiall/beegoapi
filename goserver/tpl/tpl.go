package tpl

import (
	"fmt"
	"html/template"
)

var tempPaths = []string{"html/index", "html/message"}
var templates = make(map[string]*template.Template)

func init() {
	for _, v := range tempPaths {
		templates[v] = template.Must(template.ParseFiles(fmt.Sprintf("%v.html", v)))
	}
}

func GetTemplates() *map[string]*template.Template {
	return &templates
}
