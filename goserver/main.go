package main

import (
	"github.com/astaxie/beego/logs"
	"myapp/web/beegoapi/goserver/controllers"
	"net/http"
)

var log = logs.NewLogger(10000)

func init() {
	log.SetLogger("console", "")
}
func main() {
	log.Debug("程序开始运行")
	http.Handle("/", http.FileServer(http.Dir("static")))
	http.Handle("/css/", http.FileServer(http.Dir("static")))
	http.Handle("/js/", http.FileServer(http.Dir("static")))
	http.HandleFunc("/book", controllers.BookController)
	err := http.ListenAndServe(":9090", nil)
	if err != nil {
		log.Debug("程序运行出错")

	}

}
