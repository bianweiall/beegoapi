package models

import (
	"database/sql"
	"github.com/astaxie/beego/logs"
	_ "github.com/lib/pq"
)

var log = logs.NewLogger(10000)

func init() {
	log.SetLogger("console", "")
}

func openDB() (*sql.DB, error) {
	db, err := sql.Open("postgres", "user=greenerp password=guotinghuayuan30301 dbname=beegoapi sslmode=disable")
	return db, err
}

func closeDB(db *sql.DB) error {
	return db.Close()
}
