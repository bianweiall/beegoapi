package controllers

import (
	"encoding/json"
	"fmt"
	"myapp/web/beegoapi/goserver/models"
	//"myapp/web/beegoapi/goserver/tpl"
	"net/http"
	//"reflect"
	"strconv"
	"strings"
)

var JsonErrorData *[]byte

func init() {
	data, _ := json.Marshal(struct{ Error string }{Error: "url传入的参数格式错误"})
	JsonErrorData = &data
}

func BookController(rw http.ResponseWriter, r *http.Request) {
	rw.Header().Set("Content-type", "application/json;charset=utf-8")
	switch r.Method {
	case "GET":
		var ids []int
		var fields []string
		if err := r.ParseForm(); err == nil {
			rs := r.Form
			for k, v := range rs {
				if strings.ToLower(k) == "id" {
					if val, err := strconv.Atoi(v[0]); err == nil {
						ids = append(ids, val)
					}
				} else if strings.ToLower(k) == "fields" {
					for _, val := range strings.Split(v[0], ",") {
						val = strings.ToLower(val)
						prefix := string(val[0])
						val = strings.TrimPrefix(val, prefix)
						val = fmt.Sprintf("%v%v", strings.ToUpper(prefix), val)
						fields = append(fields, val)
					}
				}
			}
			if len(ids) != 1 {
				rw.Write(*JsonErrorData)
				return
			}
		}

		m, err := models.GetBookById(ids[0], fields)
		if err != nil {
			rw.Write(*JsonErrorData)
			return
		} else {
			jsonData, _ := json.Marshal(*m)
			rw.Write(jsonData)
		}

		// if err != nil {
		// 	var t = (*tpl.GetTemplates())["html/message"]
		// 	t.Execute(rw, err.Error())
		// 	return
		// } else {
		// 	var t = (*tpl.GetTemplates())["html/index"]
		// 	t.Execute(rw, book)
		// }
	case "POST":

		models.CreateBook()
	}
	// if r.Method == "GET" {
	// 	models.CreateBook()
	// 	var ids []int
	// 	var fields []string
	// 	if err := r.ParseForm(); err == nil {
	// 		rs := r.Form
	// 		for k, v := range rs {
	// 			if strings.ToLower(k) == "id" {
	// 				if val, err := strconv.Atoi(v[0]); err == nil {
	// 					ids = append(ids, val)
	// 				}
	// 			} else if strings.ToLower(k) == "fields" {
	// 				for _, val := range strings.Split(v[0], ",") {
	// 					val = strings.ToLower(val)
	// 					prefix := string(val[0])
	// 					val = strings.TrimPrefix(val, prefix)
	// 					val = fmt.Sprintf("%v%v", strings.ToUpper(prefix), val)
	// 					fields = append(fields, val)
	// 				}
	// 			}
	// 		}
	// 		if len(ids) != 1 {
	// 			rw.Write(*JsonErrorData)
	// 			return
	// 		}
	// 	}

	// 	m, err := models.GetBookById(ids[0], fields)
	// 	if err != nil {
	// 		rw.Write(*JsonErrorData)
	// 		return
	// 	} else {
	// 		jsonData, _ := json.Marshal(*m)
	// 		rw.Write(jsonData)
	// 	}

	// 	// if err != nil {
	// 	// 	var t = (*tpl.GetTemplates())["html/message"]
	// 	// 	t.Execute(rw, err.Error())
	// 	// 	return
	// 	// } else {
	// 	// 	var t = (*tpl.GetTemplates())["html/index"]
	// 	// 	t.Execute(rw, book)
	// 	// }
	// }
}
